package com.ethor.logging.serilogj.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Domain model for communicating an event from AppStore to ElasticSearch.
 */
public class AppStoreEvent<T> {
    @JsonIgnore
    private final UUID id;
    @JsonProperty(value = "timestamp", required = true)
    private final long timestamp;
    @JsonProperty(value = "location", required = true)
    private final GeoPoint location;
    @JsonProperty(value = "hostname", required = true)
    private final String hostname;
    @JsonProperty(value = "ip_address", required = true)
    private final String ipAddress;
    @JsonProperty(value = "storeId")
    private final UUID storeId;
    @JsonProperty(value = "message")
    private final String message;
    @JsonProperty(value = "city", required = true)
    private final String city;
    @JsonProperty(value = "state", required = true)
    private final String state;
    @JsonProperty(value = "testObject", required = true)
    private final T richLog;

    public AppStoreEvent(final UUID id,
                         final long timestamp,
                         final GeoPoint location,
                         final String hostname,
                         final String ipAddress,
                         final UUID storeId,
                         final String message,
                         final String city,
                         final String state,
                         final T richLog) {
        this.id = checkNotNull(id, "id cannot be null");
        checkArgument(timestamp > 0, "timestamp must be > 0");
        this.timestamp = timestamp;
        this.location = location;
        this.hostname = checkNotNull(hostname, "hostname cannot be null");
        this.ipAddress = checkNotNull(ipAddress, "ipAddress cannot be null");
        this.storeId = storeId;
        this.message = message;
        this.city = checkNotNull(city, "city cannot be null");
        this.state = checkNotNull(state, "state cannot be null");
        this.richLog = checkNotNull(richLog, "richLog cannot be null");
    }

    public UUID getId() {
        return id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public String getHostname() {
        return hostname;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public UUID getStoreId() {
        return storeId;
    }

    public String getMessage() {
        return message;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public T getRichLog() {
        return richLog;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final AppStoreEvent<?> that = (AppStoreEvent<?>) o;

        if (timestamp != that.timestamp) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (hostname != null ? !hostname.equals(that.hostname) : that.hostname != null) return false;
        if (ipAddress != null ? !ipAddress.equals(that.ipAddress) : that.ipAddress != null) return false;
        if (storeId != null ? !storeId.equals(that.storeId) : that.storeId != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        return richLog != null ? richLog.equals(that.richLog) : that.richLog == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (hostname != null ? hostname.hashCode() : 0);
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        result = 31 * result + (storeId != null ? storeId.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (richLog != null ? richLog.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AppStoreEvent{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", location=" + location +
                ", hostname='" + hostname + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", storeId=" + storeId +
                ", message='" + message + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", richLog=" + richLog +
                '}';
    }

    public static class Builder<T> {
        private UUID id;
        private long timestamp;
        private GeoPoint location;
        private String hostname;
        private String ipAddress;
        private UUID storeId;
        private String message;
        private String city;
        private String state;
        private T richLog;

        public UUID getId() {
            return id;
        }

        public Builder<T> setId(final UUID id) {
            this.id = id;
            return this;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public Builder<T> setTimestamp(final long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public GeoPoint getLocation() {
            return location;
        }

        public Builder<T> setLocation(final GeoPoint location) {
            this.location = location;
            return this;
        }

        public String getHostname() {
            return hostname;
        }

        public Builder<T> setHostname(final String hostname) {
            this.hostname = hostname;
            return this;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public Builder<T> setIpAddress(final String ipAddress) {
            this.ipAddress = ipAddress;
            return this;
        }

        public UUID getStoreId() {
            return storeId;
        }

        public Builder<T> setStoreId(final UUID storeId) {
            this.storeId = storeId;
            return this;
        }

        public String getMessage() {
            return message;
        }

        public Builder<T> setMessage(final String message) {
            this.message = message;
            return this;
        }

        public String getCity() {
            return city;
        }

        public Builder<T> setCity(final String city) {
            this.city = city;
            return this;
        }

        public String getState() {
            return state;
        }

        public Builder<T> setState(final String state) {
            this.state = state;
            return this;
        }

        public T getRichLog() {
            return richLog;
        }

        public Builder<T> setRichLog(final T richLog) {
            this.richLog = richLog;
            return this;
        }

        public AppStoreEvent<T> build() {
            return new AppStoreEvent<>(
                    id,
                    timestamp,
                    location,
                    hostname,
                    ipAddress,
                    storeId,
                    message,
                    city,
                    state,
                    richLog
            );
        }
    }

    public static <T> Builder<T> builder() {
        return new Builder<>();
    }
}
