package com.ethor.logging.serilogj.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 */
public class GeoPoint {
    public static final GeoPoint ETHOR_CALGARY = GeoPoint.of(51.042975, -114.07803);

    @JsonProperty(value = "lat", required = true)
    private final double latitude;

    @JsonProperty(value = "lon", required = true)
    private final double longitude;

    @JsonCreator
    public GeoPoint(@JsonProperty("lat") final double latitude, @JsonProperty("lon") final double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static GeoPoint of(final double latitude, final double longitude) {
        return new GeoPoint(latitude, longitude);
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final GeoPoint geoPoint = (GeoPoint) o;

        if (Double.compare(geoPoint.latitude, latitude) != 0) return false;
        return Double.compare(geoPoint.longitude, longitude) == 0;
    }

    @Override
    public String toString() {
        return "GeoPoint{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(latitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public static class Builder {
        private double latitude;
        private double longitude;

        public double getLatitude() {
            return latitude;
        }

        public Builder setLatitude(final double latitude) {
            this.latitude = latitude;
            return this;
        }

        public double getLongitude() {
            return longitude;
        }

        public Builder setLongitude(final double longitude) {
            this.longitude = longitude;
            return this;
        }

        public GeoPoint build() {
            return new GeoPoint(latitude, longitude);
        }
    }

    public static Builder builder() {
        return new Builder();
    }
}
