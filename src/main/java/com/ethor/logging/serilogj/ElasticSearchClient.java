package com.ethor.logging.serilogj;

import com.ethor.logging.serilogj.model.AppStoreEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 */
public class ElasticSearchClient {
    private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchClient.class);

    private final String username = "john";
    private final String password = "secret";
    private final Map<String, String> requestParams = Collections.emptyMap();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final SSLContext tlsContext = buildTLSContext();

    // /alexdevindex/appstoreevent/
    private final String index;
    private final String event;
    private final String path;

    private final RestClientBuilder.HttpClientConfigCallback httpClientConfigCallback = httpClientBuilder -> {
        httpClientBuilder.setSSLContext(tlsContext);
        final BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        final UsernamePasswordCredentials credentials =
                new UsernamePasswordCredentials(username, password);
        credentialsProvider.setCredentials(AuthScope.ANY, credentials);
        httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
        return httpClientBuilder;
    };

    private final RestClientBuilder clientBuilder =
            RestClient.builder(new HttpHost("elastic.ethor.com", 8081, "https"))
                      .setHttpClientConfigCallback(httpClientConfigCallback);


    public ElasticSearchClient(final String index, final String event) {
        this.index = checkNotNull(index, "index cannot be null");
        this.event = checkNotNull(event, "event cannot be null");
        this.path = "/" + index + "/" + event + "/";
    }


    public <T> Response post(final AppStoreEvent<T> event) {
        final String json;
        try {
            json = objectMapper.writeValueAsString(event);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Could not translate event to json: " + event, e);
        }

        final RestClient client = clientBuilder.build();
        try {
            final Response response;
            try {
                return client.performRequest("post",
                                                 path + event.getId().toString(),
                                                 requestParams,
                                                 new StringEntity(json, Charset.defaultCharset()));
            } catch (IOException e) {
                throw new RuntimeException("Could not send request to ElasticSearch: " + json, e);
            }
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                LOG.warn("Could not close ElasticSearch REST API client", e);
            }
        }
    }

    // TODO need to check that each message in the event list has absolutely ZERO newlines as they are the delimiters for ES
    public <T> Response postBulk(final List<AppStoreEvent<T>> eventList) {
        final String bulkJsonLines =
                eventList.stream()
                         .map(event -> {
                             try {
                                 return "{\"create\":{\"_index\":\"" + index + "\",\"_type\":\"" + this.event +
                                         "\",\"_id\":\"" + event.getId() + "\"}}\n" +
                                         objectMapper.writeValueAsString(event);
                             } catch (JsonProcessingException e) {
                                 throw new IllegalArgumentException("Could not translate event to json: " + event, e);
                             }
                         })
                         .collect(Collectors.joining("\n"));

        LOG.debug("Posting bulk:\n{}", bulkJsonLines);

        final RestClient client = clientBuilder.build();
        try {
            return client.performRequest("post",
                                         path + "_bulk",
                                         requestParams,
                                         new StringEntity(bulkJsonLines,
                                                          Charset.defaultCharset()));
        } catch (IOException e) {
            throw new RuntimeException("Could not send bulk request to ElasticSearch", e);
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                LOG.warn("Could not close ElasticSearch REST API client", e);
            }
        }
    }

    public Response get(final UUID id) {
        final RestClient client = clientBuilder.build();

        try {
            return client.performRequest("get", path + id);
        } catch (IOException e) {
            throw new RuntimeException("Could not get resource of " + id, e);
        }
    }

    private SSLContext buildTLSContext() {
        try {
            final URL certKeystore = getClass().getResource("/ethor-ca-bundle.jks");
            if (certKeystore == null) {
                throw new IllegalStateException("eThore certificate keystore not found");
            }
            final KeyStore jks = KeyStore.getInstance("jks");
            final InputStream keystoreInputStream = certKeystore.openStream();
            jks.load(keystoreInputStream, "changeit".toCharArray());
            keystoreInputStream.close();
            final TrustManagerFactory trustManagerFactory =
                    TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(jks);
            final TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();

            final KeyManagerFactory keyManagerFactory =
                    KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(jks, "changeit".toCharArray());
            final KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();

            final SSLContext tls = SSLContext.getInstance("TLS");
            tls.init(keyManagers, trustManagers, null);
            return tls;
        } catch (Exception e) {
            throw new IllegalStateException("Could not build SSL context", e);
        }
    }

    public List<Response> postAllOneAtATime(final List<AppStoreEvent<JsonNode>> eventList) {
        final List<Response> responses = Lists.newArrayListWithCapacity(eventList.size());
        for (final AppStoreEvent<JsonNode> jsonNodeAppStoreEvent : eventList) {
            responses.add(post(jsonNodeAppStoreEvent));
        }

        return responses;
    }
}
