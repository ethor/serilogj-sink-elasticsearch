package com.ethor.logging.serilogj;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 */
public class ObjectMappers {
    private static final ObjectMapper defaultMapper = new ObjectMapper();

    public static ObjectMapper getDefault() {
        return defaultMapper;
    }
}
