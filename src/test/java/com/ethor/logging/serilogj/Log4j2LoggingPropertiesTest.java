package com.ethor.logging.serilogj;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 */
public class Log4j2LoggingPropertiesTest {
    private static final Logger LOG = LoggerFactory.getLogger(Log4j2LoggingPropertiesTest.class);

    @Test
    public void shouldLog() {
        LOG.debug("Test debug message: {}", "parameterString");
        LOG.info("Test info message: {}", "parameterString");
    }
}
