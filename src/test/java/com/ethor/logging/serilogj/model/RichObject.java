package com.ethor.logging.serilogj.model;

import java.util.UUID;

/**
 */
public class RichObject {
    private UUID id;
    private String message;

    public RichObject(final UUID id, final String message) {
        this.id = id;
        this.message = message;
    }

    public RichObject(final String message) {
        this.id = UUID.randomUUID();
        this.message = message;
    }

    public UUID getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final RichObject that = (RichObject) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return message != null ? message.equals(that.message) : that.message == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RichObject{" +
                "id=" + id +
                ", message='" + message + '\'' +
                '}';
    }
}