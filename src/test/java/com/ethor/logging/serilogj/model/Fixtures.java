package com.ethor.logging.serilogj.model;

import java.util.UUID;

/**
 */
public class Fixtures {
    public static AppStoreEvent<RichObject> buildDefaultRichEvent() {
        return buildRichEvent("I hope this gets to ElasticSearch",
                              new RichObject(UUID.fromString("d2e4e98c-ccb5-4cba-a996-32e0de585634"),
                                             "Something happened... :P"));
    }

    private static AppStoreEvent<RichObject> buildRichEvent(final String message, final RichObject richLog) {
        return AppStoreEvent.<RichObject>builder()
                .setTimestamp(System.currentTimeMillis())
                .setLocation(GeoPoint.ETHOR_CALGARY)
                .setHostname("myHost")
                .setIpAddress("10.0.1.182")
                .setStoreId(UUID.fromString("dedbf991-6b73-4b45-ba52-251b31e79616"))
                .setMessage(message)
                .setCity("Calgary")
                .setState("AB")
                .setRichLog(richLog)
                .build();
    }
}
