package com.ethor.logging.serilogj;

import com.ethor.logging.serilogj.model.AppStoreEvent;
import com.ethor.logging.serilogj.model.Fixtures;
import com.ethor.logging.serilogj.model.RichObject;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.elasticsearch.client.Response;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 */
public class ElasticSearchClientIT {
    private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchClientIT.class);

    private final ElasticSearchClient elasticSearchClient =
            new ElasticSearchClient("alexdevindex", "appstoreevent");

    private final ObjectMapper objectMapper = ObjectMappers.getDefault();

    @Test
    public void shouldPostSingleEvent() throws IOException {
        // john:secret
        final AppStoreEvent<RichObject> event = Fixtures.buildDefaultRichEvent();

        final String postBody = objectMapper.writeValueAsString(event);
        final HttpEntity entity = new StringEntity(postBody, "utf-8");

        LOG.debug("Request: {}\n{}", entity, postBody);

        final Response postResponse = elasticSearchClient.post(event);
        LOG.debug("Response: {}", postResponse);
        final String responseBody = extractResponseBody(postResponse);
        LOG.debug("Response entity: {}", responseBody);

        assertThat(postResponse.getStatusLine().getStatusCode(), is(201));

        final Response getResponse = elasticSearchClient.get(event.getId());
        final String getResponseBody = extractResponseBody(getResponse);
        LOG.debug("Get response: {}", getResponseBody);
        final JsonNode jsonNode = objectMapper.readTree(getResponseBody);
        final JsonNode getSource = jsonNode.get("_source");
        assertThat(getSource, is(objectMapper.readTree(postBody)));
    }

    @Test
    public void shouldPostBulk() {
        final List<AppStoreEvent<RichObject>> collection =
                IntStream.range(0, 10)
                         .mapToObj(i -> Fixtures.buildDefaultRichEvent())
                         .collect(toList());

        final Response bulkResponse = elasticSearchClient.postBulk(collection);

        assertThat(bulkResponse.getStatusLine().getStatusCode(), is(201));
    }

    private String extractResponseBody(final Response response) throws IOException {
        final InputStream content = response.getEntity().getContent();
        final StringBuilder sb = new StringBuilder();
        final byte[] buffer = new byte[4096];
        int bytesRead = 0;
        while ((bytesRead = content.read(buffer, 0, buffer.length)) >= 0) {
            sb.append(new String(buffer, 0, bytesRead, "utf-8"));
        }
        return sb.toString();
    }


}
